## Feb 18, 2016 - 1.0.1 ##
	Fixed a bug that caused a crash when invoking the gRaycast Command on windows

## Feb 18, 2016 ##
	Working raycast node
	Supports multiple meshes

## Jan 27, 2016 ##
	Initial Release with release notes