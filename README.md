# Maya Raycast Node #

## Contents ##
This plugin provides two nodes and two commands (with the same name) to easily create those nodes.

**gRaycast:**

This is a node that will cast a ray to input mesh(es) and it will spit out a translate, rotate and matrix for the hitpoint. The rotate will take the normal of the surface and an upvector into account. This can be used to easily constraint an object to the surface of some meshes while still having an easy way to animate it. eg. A car constrained to an uneven surface.

To create this node simply select your collision object(s) followed by the object you wish to constrain and run "gRaycast -ct -cr" or go to the "constraint" menu in maya and click "Raycast". If you run the command without providing the -ct and/or -cr flags it will connect all selected objects as input meshes

Command Flags:

  * -n/-name: The name you wish to give to the gRaycast node
  * -ct/-constrainTranslate: Will connect the outTranslate of the gRaycast node to the last object you provided to the command
  * -cr/-constrainRotate: Will connect the outRotate of the gRaycast node to the last object you provided to the command

**gCollisionDeformer:**

This is a deformer which will collide your mesh with another mesh and bulge out around the intersection like you would expect when putting pressure on a soft object.
eg. a car tire that gets pressed onto the road by the weight of the car.

To create this node simply select your collision object followed by the mesh you wish to deform and run "gCollisionDeformer" or go to the create deformers menu in maya and click "CollisionDeformer".

Command Flags:

  * -n/-name: The name you wish to give to the gCollisionDeformer node

## NOTES ##