/*
 * snapToMesh.h
 *
 *      Author: hans.cr
 */

#ifndef SNAPTOMESH_H
#define SNAPTOMESH_H

#include <maya/MPxCommand.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MDagPath.h>
#include <maya/MDGModifier.h>
#include <maya/MSelectionList.h>
#include <maya/MVector.h>
#include <maya/MString.h>
#include <maya/MPlug.h>

#include <maya/MFnDagNode.h>
#include <maya/MFnDependencyNode.h>

class SnapToMeshCommand : public MPxCommand
{
public:
	SnapToMeshCommand();
	virtual 		~SnapToMeshCommand();
	static	void*	creator();
	static	MSyntax	newSyntax();
	virtual MStatus	doIt( const MArgList& argList );
	virtual MStatus	redoIt();
	virtual MStatus	undoIt();
	virtual bool	isUndoable() const;


private:
	bool queryFlagSet;
	MSelectionList sList;

	MStatus getMeshShapeNode( MDagPath& path );
};

#endif
