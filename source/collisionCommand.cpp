/*
 * raycastCommand.cpp
 *
 *      Author: hans.cr
 */

#include "collisionCommand.h"
#include "collisionDeformer.h"

#include <maya/MItDependencyGraph.h>

using namespace std;

CollisionCommand::CollisionCommand()
{
}

CollisionCommand::~CollisionCommand()
{
}

void* CollisionCommand::creator()
{
	return new CollisionCommand;
}

bool CollisionCommand::isUndoable() const
{
	return true;
}

MSyntax CollisionCommand::newSyntax()
{
	MSyntax syntax;

	// input flags
	syntax.addFlag("-n", "-name", MSyntax::kString);

	// define behaviour of command argument that specifies the collision node:
	syntax.useSelectionAsDefault(true);
	syntax.setObjectType(MSyntax::kSelectionList, 2, 2);

	syntax.enableEdit(false);
	syntax.enableQuery(false);

	return syntax;
}

// PARSE THE COMMAND'S FLAGS AND ARGUMENTS AND STORING THEM AS PRIVATE DATA, THEN DO THE WORK BY CALLING redoIt():

MStatus CollisionCommand::doIt(const MArgList& argList)
{

	MStatus status;

	// Read all the flag arguments
	MArgDatabase argData( syntax(), argList, &status );
	CHECK_MSTATUS_AND_RETURN_IT( status );

	status = argData.getObjects( sList );
	CHECK_MSTATUS_AND_RETURN_IT( status );

	status = sList.getDagPath( 0, m_pathColliderMesh );
	CHECK_MSTATUS_AND_RETURN_IT( status );
	status = sList.getDagPath( 1, m_pathDeformMesh );
	CHECK_MSTATUS_AND_RETURN_IT( status );
	status = getMeshShapeNode(m_pathDeformMesh);
	CHECK_MSTATUS_AND_RETURN_IT( status );

	if ( argData.isFlagSet( "-n" ) )
	{
	   m_name = argData.flagArgumentString( "-n", 0, &status );
	   CHECK_MSTATUS_AND_RETURN_IT( status );
	}
	else
	{
	   m_name = "gCollisionDeformer#";
	}

	char buffer[512];
	sprintf( buffer, "deformer -type gCollisionDeformer -n \"%s\" %s", m_name.asChar(),
		   m_pathDeformMesh.partialPathName().asChar() );
	status = m_dgMod.commandToExecute( buffer );
	CHECK_MSTATUS_AND_RETURN_IT( status );

	return redoIt();

}


MStatus CollisionCommand::redoIt()
{
    MStatus status;

    status = m_dgMod.doIt();
    CHECK_MSTATUS_AND_RETURN_IT(status);

    MObject oDeformer;
	status = getDeformerFromBaseMesh( oDeformer );
	CHECK_MSTATUS_AND_RETURN_IT( status );

	MPlug colliderPlug( oDeformer, CollisionDeformer::aCollider );
	MPlug colliderMatrixPlug( oDeformer, CollisionDeformer::aColliderMatrix );
	MPlug colliderBBoxPlug( oDeformer, CollisionDeformer::aColliderBBoxSize );

	MFnDagNode dagfnMesh;

	unsigned int instanceNumber;

	status = sList.getDagPath(0, m_pathColliderMesh);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	dagfnMesh.setObject(m_pathColliderMesh);

	MPlug bboxPlug = dagfnMesh.findPlug("boundingBoxSize");


	status = m_dgMod.connect(bboxPlug, colliderBBoxPlug);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	status = getMeshShapeNode(m_pathColliderMesh);
	if(status == MStatus::kFailure){
		sList.getDagPath(0, m_pathColliderMesh);
		MGlobal::displayError(MString("Could not find a mesh for node: ") + m_pathColliderMesh.fullPathName());
	}
	instanceNumber = m_pathColliderMesh.instanceNumber();

	dagfnMesh.setObject(m_pathColliderMesh);
	MPlug meshPlug = dagfnMesh.findPlug("outMesh");
	MPlug matrixPlug = dagfnMesh.findPlug("parentInverseMatrix").elementByLogicalIndex(instanceNumber);
	status = m_dgMod.connect(meshPlug, colliderPlug);
	status = m_dgMod.connect(matrixPlug, colliderMatrixPlug);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	m_dgMod.doIt();


	// SET COMMAND RESULT TO BE NEW NODE'S NAME, AND RETURN:
	setResult(m_name);

	return MStatus::kSuccess;
}

MStatus CollisionCommand::getMeshShapeNode( MDagPath& path )
{
	MStatus status;

	if( path.apiType() == MFn::kMesh)
	{
		return MS::kSuccess;
	}

	unsigned int numShapes;
	status = path.numberOfShapesDirectlyBelow( numShapes );
	CHECK_MSTATUS_AND_RETURN_IT(status);
	for(unsigned int i=0; i<numShapes; i++){
		status = path.extendToShapeDirectlyBelow( i );
		CHECK_MSTATUS_AND_RETURN_IT(status);

		if( !path.hasFn(MFn::kMesh)){
			path.pop();
			continue;
		}

		MFnDagNode fnNode( path, &status );
		CHECK_MSTATUS_AND_RETURN_IT(status);
		if( !fnNode.isIntermediateObject()){
			return MS::kSuccess;
		}
		path.pop();

	}

	return MS::kFailure;
}

MStatus CollisionCommand::getDeformerFromBaseMesh( MObject& oDeformer )
{
    MStatus status;
    MObject oBaseMesh = m_pathDeformMesh.node();
    MItDependencyGraph itGraph( oBaseMesh, MFn::kInvalid, MItDependencyGraph::kUpstream,
        MItDependencyGraph::kDepthFirst, MItDependencyGraph::kNodeLevel, &status );
    CHECK_MSTATUS_AND_RETURN_IT( status );

    while ( !itGraph.isDone() )
    {
    	oDeformer = itGraph.currentItem();
        MFnDependencyNode fnNode( oDeformer, &status );
        CHECK_MSTATUS_AND_RETURN_IT( status );
        if ( fnNode.typeId() == CollisionDeformer::id )
        {
            return MS::kSuccess;
        }
        itGraph.next();
    }
    return MS::kFailure;
}

MStatus CollisionCommand::undoIt()
{
    MStatus status;

    // Restore the initial state
    status = m_dgMod.undoIt();
    CHECK_MSTATUS_AND_RETURN_IT( status );

    return MS::kSuccess;
}
