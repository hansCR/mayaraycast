/*
 * collisionDeformer.h
 *
 *  Created on: Feb 29, 2016
 *      Author: hans.cr
 */

#ifndef COLLISIONDEFORMER_H
#define COLLISIONDEFORMER_H

#include <maya/MPxDeformerNode.h>

#include <maya/MTypeId.h>
#include <maya/MGlobal.h>
#include <maya/MObject.h>
#include <maya/MDagPath.h>
#include <maya/MBoundingBox.h>

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>

#include <maya/MVector.h>
#include <maya/MPoint.h>
#include <maya/MMatrix.h>
#include <maya/MFloatMatrix.h>
#include <maya/MPointArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MRampAttribute.h>

#include <maya/MMessage.h>
#include <maya/MNodeMessage.h>


#include <maya/MMeshIntersector.h>
#include <maya/MFnMesh.h>
#include <maya/MItGeometry.h>

#include <maya/MAnimControl.h>

class CollisionDeformer : MPxDeformerNode
{
public:
						CollisionDeformer();
	virtual void		postConstructor();
	virtual				~CollisionDeformer();

	static  void*		creator();


	virtual	MStatus		setDependentsDirty(const MPlug& inPlug,
							MPlugArray& affectedPlugs);
	virtual MStatus		deform( MDataBlock& data, MItGeometry& itGeo,
			const MMatrix& localToWorldMatrix, unsigned int geomIndex );
	static  MStatus		initialize();


    static	MObject		aCollider;
    static	MObject		aStartFrame;
    static	MObject		aBulgeExtend;
    static	MObject		aBulge;
    static	MObject		aOffset;
    static	MObject		aColliderBBoxSize;
    static	MObject		aColliderMatrix;
    static	MObject		aBulgeShape;
    static	MObject		aBackFace;
    static	MObject		aSculptMode;

	static	MTypeId		id;

private:
	MMeshIsectAccelParams mmAccelParams;
	MMeshIntersector intersector;
	MFloatPointArray pointPositions;
	MFloatVectorArray pointNormals;
	MFloatArray bulgeParameter;
	MPointArray deformedPoints;
	bool m_initialized, m_dirtyCollider, m_dirtyInput;

};



#endif
