/*
 * raycastLocator.cpp
 *
 *      Author: hans.cr
 */

#include "raycastLocator.h"

#include <ctime>

#define MERR_CHK(status,msg) if ( !status ) { MGlobal::displayError(msg); } // cerr << msg << endl; }

#define ROTATE_ORDER_XYZ        0
#define ROTATE_ORDER_YZX        1
#define ROTATE_ORDER_ZXY        2
#define ROTATE_ORDER_XZY        3
#define ROTATE_ORDER_YXZ        4
#define ROTATE_ORDER_ZYX        5


using namespace std;
using namespace MathUtils;



MTypeId     RaycastLocator::id( 0x00125609 );

MObject     RaycastLocator::aOutTranslate;
MObject     RaycastLocator::aRotateX;
MObject     RaycastLocator::aRotateY;
MObject     RaycastLocator::aRotateZ;
MObject     RaycastLocator::aOutRotate;
MObject     RaycastLocator::aOutMatrix;
MObject     RaycastLocator::aIsHit;
MObject     RaycastLocator::aParamLen;

MObject     RaycastLocator::aRayLen;
MObject		RaycastLocator::aMeshes;
MObject     RaycastLocator::aTargetParentInverse;
MObject     RaycastLocator::aTargetRotateOrder;
MObject     RaycastLocator::aInMatrix;
MObject     RaycastLocator::aAim;
MObject     RaycastLocator::aUp;
MObject     RaycastLocator::aWorldUp;
MObject     RaycastLocator::aNormal;


static float arrow[][3] = { { 0.0f,  0.0f,  1.0f  },
							{ 0.0f, -0.2f,  0.7f  },
							{ 0.0f, -0.08f, 0.75f },
							{ 0.0f, -0.1f,  0.0f  },
							{ 0.0f,  0.1f,  0.0f  },
							{ 0.0f,  0.1f,  0.0f  },
							{ 0.0f,  0.08f, 0.75f },
							{ 0.0f,  0.2f,  0.7f  }
};

static int arrowCount = 8;




RaycastLocator::RaycastLocator()
{
	m_initialized = false;
}


void RaycastLocator::postConstructor()
{
    MObject oThis = thisMObject();
    MFnDependencyNode fnNode( oThis );
    fnNode.setName( "rayCastShape#" );
}


RaycastLocator::~RaycastLocator() 
{
}

bool RaycastLocator::isBounded() const
{
	return true;
}

MBoundingBox RaycastLocator::boundingBox() const
{
	MPoint corner1 = MPoint(0,0.2,1);
	MPoint corner2 = MPoint(0,-0.2,0);

	return MBoundingBox(corner1, corner2);
}


void* RaycastLocator::creator()
{
    return new RaycastLocator();
}


MStatus	RaycastLocator::setDependentsDirty(const MPlug& plug, MPlugArray& affectedPlugs)
{
	MStatus status;

	MObject oThis = thisMObject();

	if(plug == aTargetRotateOrder){
		m_dirtyRoO = true;
	}


	if(plug == aMeshes ){
		unsigned int logicalIndex = plug.logicalIndex();
		m_dirtyMeshes[logicalIndex] = true;
	}

	return MS::kSuccess;
}


MStatus RaycastLocator::compute( const MPlug& plug, MDataBlock& data )

{
	MStatus status;

	if(plug == aOutMatrix ||
			plug == aOutTranslate || plug.parent() == aOutTranslate ||
			plug == aOutRotate || plug.parent() == aOutRotate ||
			plug == aIsHit || plug == aParamLen){

		MDataHandle hOutPoint	= data.outputValue( aOutTranslate );
		MDataHandle hOutRotate	= data.outputValue( aOutRotate );
		MDataHandle hOutMatrix	= data.outputValue( aOutMatrix );
		MDataHandle hIsHit		= data.outputValue( aIsHit );
		MDataHandle hParamLen	= data.outputValue( aParamLen );

		MFnMesh fnMesh;

		double duration;
		clock_t start = clock();


		// Read inputs
		MMatrix sourceMatrix = data.inputValue( aInMatrix, &status ).asMatrix();

		if(! data.isClean(aOutTranslate)){

			float fRaylen = data.inputValue( aRayLen, &status ).asFloat();

			vAim  		= MVector(data.inputValue( aAim, &status ).asFloatVector());
			mParentInverse = MMatrix(data.inputValue( aTargetParentInverse, &status ).asMatrix());

			MArrayDataHandle hMeshes = data.inputArrayValue( aMeshes, &status );
			unsigned int numMeshes = hMeshes.elementCount();
			unsigned int logicalIndex;

			// If no mesh is attached do not perform any calculation
			// and set default positions for drawing

			if(numMeshes == 0){
				return MS::kSuccess;
			}


			// Get source position of ray
			MPoint pSource = MPoint(0,0,0,1)*sourceMatrix;
			fpSource = MFloatPoint(pSource);

			// cout << vAim << endl;

			// Get direction of ray
			MFloatVector fvRayDir = MFloatVector(vAim * sourceMatrix);

			MObject oMesh;
			MDagPath dagMesh;
			MFloatPoint fpTempHit;

			// Vector position(pSource);
			// Vector direction(fvRayDir);

			// Ray worldRay(position, direction);

			float fHitRayParam, fHitBary1, fHitBary2;
			int nHitFace, nHitTriangle;

			float fClosestHit = fRaylen+1;

			for(unsigned int i = 0; i < numMeshes; i++ )
			{
				hMeshes.jumpToElement(i);
				logicalIndex = hMeshes.elementIndex();

				// If Mesh is dirty, rebuild accelparams
				if (m_dirtyMeshes[logicalIndex] || !m_initialized) {
					oMesh = hMeshes.inputValue().asMesh();

					if (oMesh == MObject::kNullObj) {
						continue;
					}
					// cout << "rebuilding Mesh " << logicalIndex << endl;
					m_meshes[logicalIndex] = oMesh;
					status = fnMesh.setObject(oMesh);
					CHECK_MSTATUS_AND_RETURN_IT(status);
					
					m_accelParams[logicalIndex] = fnMesh.autoUniformGridParams();
					m_dirtyMeshes[logicalIndex] = false;
				}
				status = fnMesh.setObject(m_meshes[logicalIndex]);
				CHECK_MSTATUS_AND_RETURN_IT(status);

				// MERR_CHK(MDagPath::getAPathTo(mesh, dagMesh), "couldn't get a path");

				// Ray localRay(worldRay);

				// Perform raycast to mesh
				

				bool bHit = fnMesh.closestIntersection( fpSource, fvRayDir, NULL, NULL, false,
						MSpace::kWorld, fRaylen, false, &m_accelParams[logicalIndex], fpTempHit, &fHitRayParam, &nHitFace, &nHitTriangle,
						&fHitBary1, &fHitBary2, (float)1e-6, &status);

				CHECK_MSTATUS_AND_RETURN_IT(status);

				if(!bHit){
					continue;
				}

				if(fHitRayParam < fClosestHit){
					fClosestHit = fHitRayParam;
					closestIndex = logicalIndex;
					fpHit = fpTempHit;
				}

			}

			if(fClosestHit > fHitRayParam){
				hIsHit.setBool(false);
				hIsHit.setClean();
				hParamLen.setFloat(fRaylen);
				hParamLen.setClean();
				hOutPoint.setClean();
				hOutRotate.setClean();
				hOutMatrix.setClean();


				duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
				// cout << "duration: " << duration << " Not hit"<< endl;

				return MS::kSuccess;
			}

			MPoint outPoint = MPoint(fpHit) * mParentInverse;

			hOutPoint.set3Float( (float)outPoint.x, (float)outPoint.y, (float)outPoint.z );
			hOutPoint.setClean();
			hIsHit.set(true);
			hIsHit.setClean();
			hParamLen.setFloat(fClosestHit);
			hParamLen.setClean();

		}

		duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
		// cout << "duration: " << duration << endl;


		if(plug == aOutMatrix || plug == aOutRotate || plug.parent() == aOutRotate){

			status = fnMesh.setObject(m_meshes[closestIndex]);
			CHECK_MSTATUS_AND_RETURN_IT(status);

			vUpIn 		= data.inputValue( aUp, &status ).asVector();
			vWorldUp 	= data.inputValue( aWorldUp, &status ).asVector();
			vNormal		= data.inputValue( aNormal, &status ).asVector();

			short order	= data.inputValue( aTargetRotateOrder, &status ).asShort();
            switch ( order ) {
                    case ROTATE_ORDER_XYZ:
                        roo = MTransformationMatrix::kXYZ; break;
                    case ROTATE_ORDER_YZX:
                    	roo = MTransformationMatrix::kYZX; break;
                    case ROTATE_ORDER_ZXY:
                    	roo = MTransformationMatrix::kZXY; break;
                    case ROTATE_ORDER_XZY:
                    	roo = MTransformationMatrix::kXZY; break;
                    case ROTATE_ORDER_YXZ:
                    	roo = MTransformationMatrix::kYXZ; break;
                    case ROTATE_ORDER_ZYX:
                    	roo = MTransformationMatrix::kZYX; break;
                    default:
                    	roo = MTransformationMatrix::kInvalid; break;
            }

			vUpIn.normalize();
			vWorldUp.normalize();
			vNormal.normalize();

			vWorldUp *= sourceMatrix;
			MVector vFront = vWorldUp^vNormal;
			MVector vUp = vNormal^vFront;

			MMatrix test;
			MatrixUtils::CreateMatrix(MPoint(0,0,0,1), vUp, vNormal, vFront, test);
			vUp = vUpIn * test;
			vFront = vUp^vNormal;
			MatrixUtils::CreateMatrix(MPoint(0,0,0,1), vUp, vNormal, vFront, test);

			// Get Normal of mesh on hitpoint
			MVector vHitNormal;
			status = fnMesh.getClosestNormal(fpHit, vHitNormal, MSpace::kWorld, NULL, &m_accelParams[closestIndex]);


			// Get Up vector of ray
			MVector vHitWorldUp = MVector(1.0, 0.0, 0.0) * sourceMatrix;
			MVector vHitFront = vHitNormal^vHitWorldUp;
			MVector vHitUp = vHitFront^vHitNormal;

			MMatrix outMatrix;

			MatrixUtils::CreateMatrix(fpHit, vHitUp, vHitNormal, vHitFront, outMatrix);
			// outMatrix = test * outMatrix;
			MTransformationMatrix trnsfMatrix = MTransformationMatrix(outMatrix*mParentInverse);

			trnsfMatrix.reorderRotation(roo);
			double rotation[3];
			trnsfMatrix.getRotation(rotation, roo);
			hOutRotate.set( rotation[0], rotation[1], rotation[2] );
			hOutRotate.setClean();

			hOutMatrix.setMMatrix(outMatrix);
			hOutMatrix.setClean();

			m_initialized = true;
		}

	}
	else{
		return MS::kUnknownParameter;
	}


    return MS::kSuccess;
}


void RaycastLocator::draw( M3dView& view,
						  const MDagPath& DGpath,
						  M3dView::DisplayStyle style,
						  M3dView::DisplayStatus status )
{
	view.beginGL();
	glPushAttrib( GL_CURRENT_BIT );

	MColor solidColor, wireColor;
	if ( status == M3dView::kActive )
	{
		wireColor = MColor( 1.0f, 1.0f, 1.0f, 1.0f );
	}
	else if ( status == M3dView::kLead )
	{
		wireColor = MColor( .26f, 1.0f, .64f, 1.0f );
	}
	else
	{
		wireColor = MColor( 1.0f, 1.0f, 0.0f, 1.0f );
	}

	glColor4f( wireColor.r, wireColor.g, wireColor.b, wireColor.a );

	glBegin( GL_TRIANGLE_FAN );
	int i;
	int last = arrowCount;
	for ( i = 0; i < last; ++i ) {
		glVertex3f( arrow[i][0], arrow[i][1], arrow[i][2]);
	}
	glEnd();

	glPopAttrib();
	view.endGL();


}

MStatus RaycastLocator::initialize()
{
    MStatus status;

    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnUnitAttribute uAttr;



	aOutTranslate = nAttr.createPoint( "outTranslate", "outTranslate");
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	addAttribute(aOutTranslate);

    aRotateX = uAttr.create( "outRotateX", "outRotateX", MFnUnitAttribute::kAngle, 0.0 );
    nAttr.setStorable(false);
    aRotateY = uAttr.create( "outRotateY", "outRotateY", MFnUnitAttribute::kAngle, 0.0 );
    nAttr.setStorable(false);
    aRotateZ = uAttr.create( "outRotateZ", "outRotateZ", MFnUnitAttribute::kAngle, 0.0 );
    nAttr.setStorable(false);

    aOutRotate = nAttr.create( "outRotate", "outRotate", aRotateX, aRotateY, aRotateZ );
    nAttr.setStorable(false);
	nAttr.setWritable(false);
	addAttribute(aOutRotate);

	aOutMatrix = mAttr.create( "outMatrix", "outMatrix");
	mAttr.setStorable(false);
	mAttr.setWritable(false);
	addAttribute(aOutMatrix);

	aIsHit = nAttr.create( "isHit", "isHit", MFnNumericData::kBoolean);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	addAttribute(aIsHit);

	aParamLen = nAttr.create( "parametricLength", "parametricLength", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	addAttribute(aParamLen);

	aInMatrix = mAttr.create( "inMatrix", "inMatrix");
	addAttribute(aInMatrix);
	attributeAffects(aInMatrix, aOutTranslate );
	attributeAffects(aInMatrix, aOutRotate );
	attributeAffects(aInMatrix, aOutMatrix );
	attributeAffects(aInMatrix, aIsHit );
	attributeAffects(aInMatrix, aParamLen );

	aMeshes = tAttr.create("inMesh", "inMesh", MFnData::kMesh );
	tAttr.setArray(true);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setIndexMatters(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kDelete);
	addAttribute(aMeshes);
	attributeAffects(aMeshes, aOutTranslate );
	attributeAffects(aMeshes, aOutRotate );
	attributeAffects(aMeshes, aOutMatrix );
	attributeAffects(aMeshes, aIsHit );
	attributeAffects(aMeshes, aParamLen );

	aTargetParentInverse = mAttr.create( "targetParentInverseMatrix", "tpim");
	addAttribute(aTargetParentInverse);
	attributeAffects(aTargetParentInverse, aOutTranslate );
	attributeAffects(aTargetParentInverse, aOutRotate );

	aTargetRotateOrder = nAttr.create( "targetRotateOrder", "troo", MFnNumericData::kShort);
	addAttribute(aTargetRotateOrder);
	attributeAffects(aTargetRotateOrder, aOutRotate );

	aRayLen = nAttr.create( "maxLength", "maxLength", MFnNumericData::kFloat, 100.0F );
	nAttr.setMin(0.0F);
	addAttribute(aRayLen);
	attributeAffects(aRayLen, aOutTranslate );
	attributeAffects(aRayLen, aOutRotate );
	attributeAffects(aRayLen, aOutMatrix );
	attributeAffects(aRayLen, aIsHit );
	attributeAffects(aRayLen, aParamLen );

	aAim = nAttr.createPoint( "aim", "aim" );
	nAttr.setChannelBox(true);
	nAttr.setDefault(0.0F, 0.0F, 1.0F);
	addAttribute(aAim);
	attributeAffects(aAim, aOutTranslate );
	attributeAffects(aAim, aOutRotate );
	attributeAffects(aAim, aOutMatrix );
	attributeAffects(aAim, aIsHit );
	attributeAffects(aAim, aParamLen );

	aUp = nAttr.createPoint( "up", "up" );
	nAttr.setChannelBox(true);
	nAttr.setDefault(1.0F, 0.0F, 0.0F);
	addAttribute(aUp);
	attributeAffects(aUp, aOutRotate );
	attributeAffects(aUp, aOutMatrix );

	aWorldUp = nAttr.createPoint( "worldUp", "wup" );
	nAttr.setChannelBox(true);
	nAttr.setDefault(1.0F, 0.0F, 0.0F);
	addAttribute(aWorldUp);
	attributeAffects(aWorldUp, aOutRotate );
	attributeAffects(aWorldUp, aOutMatrix );

	aNormal = nAttr.createPoint( "normal", "nr" );
	nAttr.setChannelBox(true);
	nAttr.setDefault(0.0F, 1.0F, 0.0F);
	addAttribute(aNormal);
	attributeAffects(aNormal, aOutRotate );
	attributeAffects(aNormal, aOutMatrix );

	return MS::kSuccess;
}
