/*
 * raycastCommand.cpp
 *
 *      Author: hans.cr
 */

#include "snapToMesh.h"
using namespace std;

SnapToMeshCommand::SnapToMeshCommand()
{
}

SnapToMeshCommand::~SnapToMeshCommand()
{
}

void* SnapToMeshCommand::creator()
{
	return new SnapToMeshCommand;
}

bool SnapToMeshCommand::isUndoable() const
{
	return true;
}

MSyntax SnapToMeshCommand::newSyntax()
{
	MSyntax syntax;

	// define behaviour of command argument that specifies the raycast node:
	syntax.useSelectionAsDefault(true);
	syntax.setObjectType(MSyntax::kSelectionList, 2);

	syntax.enableEdit(false);
	syntax.enableQuery(false);

	return syntax;
}

// PARSE THE COMMAND'S FLAGS AND ARGUMENTS AND STORING THEM AS PRIVATE DATA, THEN DO THE WORK BY CALLING redoIt():

MStatus SnapToMeshCommand::doIt(const MArgList& argList)
{
	MStatus status;

	// Read all the flag arguments
	MArgDatabase argData( syntax(), argList, &status );
	CHECK_MSTATUS_AND_RETURN_IT( status );

	status = argData.getObjects(sList);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	// STORE FLAG INDICATORS, STORING WHETHER EACH FLAG HAS BEEN SET OR NOT:
	queryFlagSet 	= argData.isQuery();


   // STORE THE SPECIFIED OBJECT, INPUTTED FROM EITHER THE COMMAND ARGUMENT OR CURRENT SELECTION:
   argData.getObjects(sList);

   return redoIt();

}


MStatus SnapToMeshCommand::redoIt()
{
    MStatus status;

	// WHEN COMMAND *NOT* IN "QUERY MODE" (I.E. "CREATION MODE"), CREATE AND CONNECT A "closestPointOnCurve" NODE AND RETURN ITS NODE NAME:
	if (!queryFlagSet)
		{
		MDGModifier dgMod;

		dgMod.connect(parentMatrixPlug, matrixInPlug);
		dgMod.doIt();


		MDagPath dagMesh;
		MFnDagNode dagfnMesh;

		unsigned instanceNumber=0;

		for(unsigned int i=0; i<sList.length(); i++){
			status = sList.getDagPath(i, dagMesh);
			CHECK_MSTATUS_AND_RETURN_IT(status);
			status = getMeshShapeNode(dagMesh);
			CHECK_MSTATUS_AND_RETURN_IT(status);
			instanceNumber = dagMesh.instanceNumber();

			dagfnMesh.setObject(dagMesh);
			MPlug meshPlug = dagfnMesh.findPlug("worldMesh").elementByLogicalIndex(instanceNumber);
			dgMod.doIt();
			CHECK_MSTATUS_AND_RETURN_IT(status);

			cout << dagMesh.fullPathName() << endl;
		}


		// SET COMMAND RESULT TO BE NEW NODE'S NAME, AND RETURN:
		setResult(raycastNodeName);
		return MStatus::kSuccess;

	}

	return MStatus::kSuccess;
}

MStatus SnapToMeshCommand::getMeshShapeNode( MDagPath& path )
{
	MStatus status;

	if( path.apiType() == MFn::kMesh)
	{
		return MS::kSuccess;
	}

	unsigned int numShapes;
	status = path.numberOfShapesDirectlyBelow( numShapes );
	CHECK_MSTATUS_AND_RETURN_IT(status);
	for(unsigned int i=0; i<numShapes; i++){
		status = path.extendToShapeDirectlyBelow( i );
		CHECK_MSTATUS_AND_RETURN_IT(status);

		if( !path.hasFn(MFn::kMesh)){
			path.pop();
			continue;
		}

		MFnDagNode fnNode( path, &status );
		CHECK_MSTATUS_AND_RETURN_IT(status);
		if( !fnNode.isIntermediateObject()){
			return MS::kSuccess;
		}
		path.pop();

	}

	return MS::kFailure;
}

MStatus SnapToMeshCommand::undoIt()
{

	return MS::kSuccess;
}
