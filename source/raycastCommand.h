/*
 * raycastCommand.h
 *
 *      Author: hans.cr
 */

#ifndef RAYCASTCOMMAND_H
#define RAYCASTCOMMAND_H

#include <maya/MPxCommand.h>
#include <maya/MGlobal.h>
#include <maya/MIOStream.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MDagPath.h>
#include <maya/MDagModifier.h>
#include <maya/MSelectionList.h>
#include <maya/MVector.h>
#include <maya/MString.h>
#include <maya/MPlug.h>

#include <maya/MFnDagNode.h>
#include <maya/MFnDependencyNode.h>

#include "common.h"

class RaycastCommand : public MPxCommand
{
public:
					RaycastCommand();
	virtual 		~RaycastCommand();
	static	void*	creator();
	static	MSyntax	newSyntax();
	virtual MStatus	doIt( const MArgList& argList );
	virtual MStatus	redoIt();
	virtual MStatus	undoIt();
	virtual bool	isUndoable() const;


private:
	// flags
	bool nameFlagSet, aimFlagSet, upFlagSet, worldUpFlagSet, normalFlagSet,
		queryFlagSet, translateFlagSet, rotateFlagSet;
	MString m_name;			// Node Name
	MVector aim;			// Aim Vector
	MVector up;				// Up Vector
	MVector worldUp;		// World Up Vector
	MVector normal;			// normal aligned vector

	MSelectionList sList;
	MDagModifier m_dagMod;
	MFnDagNode dagfnRayShape;

	MStatus createNode();
	MStatus getMeshShapeNode( MDagPath& path );
	MStatus renameNodes(MObject transform, MString baseName);
};

#endif
