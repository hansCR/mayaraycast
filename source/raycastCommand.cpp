/*
 * raycastCommand.cpp
 *
 *      Author: hans.cr
 */

#include "raycastCommand.h"
#include "raycastLocator.h"
using namespace std;


#define checkErr(stat,msg)          \
    if ( MS::kSuccess != stat ) {   \
        displayError(MString(msg) + ": " + stat.errorString());   \
        return stat;                \
    }

RaycastCommand::RaycastCommand()
{
}

RaycastCommand::~RaycastCommand()
{
}

void* RaycastCommand::creator()
{
	return new RaycastCommand;
}

bool RaycastCommand::isUndoable() const
{
	return true;
}

MSyntax RaycastCommand::newSyntax()
{
	MSyntax syntax;

	// input flags
	syntax.addFlag("-n", "-name", MSyntax::kString);
	syntax.addFlag("-a", "-aimVector", MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);
	syntax.addFlag("-up", "-upVector", MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);
	syntax.addFlag("-wup", "-worldUp", MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);
	syntax.addFlag("-nr", "-normal", MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);
	syntax.addFlag("-ct", "-constrainTranslate");
	syntax.addFlag("-cr", "-constrainRotate");

	// define behaviour of command argument that specifies the raycast node:
	syntax.useSelectionAsDefault(true);
	syntax.setObjectType(MSyntax::kSelectionList, 1);

	syntax.enableEdit(false);
	syntax.enableQuery(false);

	return syntax;
}

MStatus RaycastCommand::createNode()
{
	MStatus status;

	// CREATE THE NODE:
	MObject oRay = m_dagMod.createNode(RaycastLocator::id, MObject::kNullObj, &status);
	checkErr(status, "Could not create the raycast node" );

	status = m_dagMod.doIt();
	checkErr(status, "Could not commit creation of the raycast node" );

	status = renameNodes(oRay, m_name);
	if (!status) return status;

	dagfnRayShape.setObject(oRay);

	MString	 rayname = dagfnRayShape.name();
	dagfnRayShape.setObject(dagfnRayShape.child(0));

	MPlug parentMatrixPlug = dagfnRayShape.findPlug(RaycastLocator::parentMatrix).elementByLogicalIndex(0);
	MPlug matrixInPlug = dagfnRayShape.findPlug(RaycastLocator::aInMatrix);

	status = m_dagMod.connect(parentMatrixPlug, matrixInPlug);
	CHECK_MSTATUS_AND_RETURN_IT( status );

	// SET THE ".aim" ATTRIBUTE, IF SPECIFIED IN THE COMMAND:
	if (aimFlagSet)
	{
		MPlug aimXPlug = dagfnRayShape.findPlug("aimX", &status);
		if(status == MStatus::kSuccess)
			aimXPlug.setValue(aim.x);
		MPlug aimYPlug = dagfnRayShape.findPlug("aimY", &status);
		CHECK_MSTATUS_AND_RETURN_IT( status );
		aimYPlug.setValue(aim.y);
		MPlug aimZPlug = dagfnRayShape.findPlug("aimZ", &status);
		CHECK_MSTATUS_AND_RETURN_IT( status );
		aimZPlug.setValue(aim.z);
		// cout << "aim set to: " << aim << endl;
	}

	// SET THE ".up" ATTRIBUTE, IF SPECIFIED IN THE COMMAND:
	if (upFlagSet)
	{
		MPlug upXPlug = dagfnRayShape.findPlug("upX", &status);
		CHECK_MSTATUS_AND_RETURN_IT( status );
		upXPlug.setValue(up.x);
		MPlug upYPlug = dagfnRayShape.findPlug("upY", &status);
		CHECK_MSTATUS_AND_RETURN_IT( status );
		upYPlug.setValue(up.y);
		MPlug upZPlug = dagfnRayShape.findPlug("upZ", &status);
		CHECK_MSTATUS_AND_RETURN_IT( status );
		upZPlug.setValue(up.z);
		// cout << "up set to: " << up << endl;
	}

	// SET THE ".worldUp" ATTRIBUTE, IF SPECIFIED IN THE COMMAND:
	if (worldUpFlagSet)
	{
		MPlug worldUpXPlug = dagfnRayShape.findPlug("worldUpX");
		worldUpXPlug.setValue(worldUp.x);
		MPlug worldUpYPlug = dagfnRayShape.findPlug("worldUpY");
		worldUpYPlug.setValue(worldUp.y);
		MPlug worldUpZPlug = dagfnRayShape.findPlug("worldUpZ");
		worldUpZPlug.setValue(worldUp.z);
		// cout << "worldUp set to: " << worldUp << endl;
	}

	// SET THE ".normal" ATTRIBUTE, IF SPECIFIED IN THE COMMAND:
	if (normalFlagSet)
	{
		MPlug normalXPlug = dagfnRayShape.findPlug("normalX");
		normalXPlug.setValue(normal.x);
		MPlug normalYPlug = dagfnRayShape.findPlug("normalY");
		normalYPlug.setValue(normal.y);
		MPlug normalZPlug = dagfnRayShape.findPlug("normalZ");
		normalZPlug.setValue(normal.z);
		// cout << "normal set to: " << normal << endl;
	}


	MDagPath dagMesh;
	MFnDagNode dagfnMesh;

	unsigned instanceNumber=0;

	MPlug meshInPlug = dagfnRayShape.findPlug("inMesh");
	MPlug translateOutPlug = dagfnRayShape.findPlug("outTranslate");
	MPlug rotateOutPlug = dagfnRayShape.findPlug("outRotate");
	MPlug parentInversePlug = dagfnRayShape.findPlug("targetParentInverseMatrix");
	MPlug rotateOrderPlug = dagfnRayShape.findPlug("targetRotateOrder");

	unsigned int inputConnections = sList.length();
	if (translateFlagSet || rotateFlagSet){
		inputConnections -= 1;
		if (inputConnections < 1){
			MGlobal::displayError(MString("Please select at least two objects when using -ct and/or -cr flag"));
			return MS::kFailure;
		}

		status = sList.getDagPath(inputConnections, dagMesh);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		if( dagMesh.apiType() != MFn::kTransform)
		{
			MGlobal::displayError(MString("last selected node is not a transform: ") + dagMesh.fullPathName());
			return MS::kFailure;
		}

		dagfnMesh.setObject(dagMesh);
		if(translateFlagSet){
			MPlug translatePlug = dagfnMesh.findPlug("translate");
			status = m_dagMod.connect(translateOutPlug, translatePlug);

		}
		if(rotateFlagSet){
			MPlug rotatePlug = dagfnMesh.findPlug("rotate");
			status = m_dagMod.connect(rotateOutPlug, rotatePlug);

		}
		MPlug pimPlug = dagfnMesh.findPlug("parentInverseMatrix", &status).elementByLogicalIndex(0);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		MPlug rooPlug = dagfnMesh.findPlug("rotateOrder", &status);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		status = m_dagMod.connect(pimPlug, parentInversePlug);
		status = m_dagMod.connect(rooPlug, rotateOrderPlug);
		CHECK_MSTATUS_AND_RETURN_IT(status);
	}


	for(unsigned int i=0; i<inputConnections; i++){
		status = sList.getDagPath(i, dagMesh);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		status = getMeshShapeNode(dagMesh);
		if(status == MStatus::kFailure){
			sList.getDagPath(i, dagMesh);
			MGlobal::displayError(MString("Could not find a mesh for node: ") + dagMesh.fullPathName());
		}
		instanceNumber = dagMesh.instanceNumber();

		dagfnMesh.setObject(dagMesh);
		MPlug meshPlug = dagfnMesh.findPlug("worldMesh").elementByLogicalIndex(instanceNumber);
		status = m_dagMod.connect(meshPlug, meshInPlug.elementByLogicalIndex(i));
		CHECK_MSTATUS_AND_RETURN_IT(status);

		// cout << dagMesh.fullPathName() << endl;
		// cout << meshPlug.name() << "to" << meshInPlug.name() << endl;
	}

	m_dagMod.doIt();

	// SET COMMAND RESULT TO BE NEW NODE'S NAME, AND RETURN:
	setResult(rayname);
	return MStatus::kSuccess;
}

// PARSE THE COMMAND'S FLAGS AND ARGUMENTS AND STORING THEM AS PRIVATE DATA, THEN DO THE WORK BY CALLING redoIt():
MStatus RaycastCommand::doIt(const MArgList& argList)
{
	MStatus status;

	// Read all the flag arguments
	MArgDatabase argData( syntax(), argList, &status );
	CHECK_MSTATUS_AND_RETURN_IT( status );

	status = argData.getObjects( sList );
	CHECK_MSTATUS_AND_RETURN_IT( status );

	// STORE FLAG INDICATORS, STORING WHETHER EACH FLAG HAS BEEN SET OR NOT:
	queryFlagSet      = argData.isQuery();
	nameFlagSet       = argData.isFlagSet("-name");
	aimFlagSet        = argData.isFlagSet("-aimVector");
	upFlagSet         = argData.isFlagSet("-upVector");
	worldUpFlagSet    = argData.isFlagSet("-worldUp");
	normalFlagSet     = argData.isFlagSet("-normal");
	translateFlagSet  = argData.isFlagSet("-constrainTranslate");
	rotateFlagSet     = argData.isFlagSet("-constrainRotate");

	// STORE THE "aim" IF SPECIFIED, OTHERWISE ASSIGN DEFAULT:
	if (aimFlagSet)
	{
		argData.getFlagArgument("-aimVector", 0, aim.x);
		argData.getFlagArgument("-aimVector", 1, aim.y);
		argData.getFlagArgument("-aimVector", 2, aim.z);
	}

	// STORE THE "up" IF SPECIFIED, OTHERWISE ASSIGN DEFAULT:
	if (upFlagSet)
	{
		argData.getFlagArgument("-upVector", 0, up.x);
		argData.getFlagArgument("-upVector", 1, up.y);
		argData.getFlagArgument("-upVector", 2, up.z);
	}

	// STORE THE "worldUp" IF SPECIFIED, OTHERWISE ASSIGN DEFAULT:
	if (worldUpFlagSet)
	{
		argData.getFlagArgument("-worldUp", 0, worldUp.x);
		argData.getFlagArgument("-worldUp", 1, worldUp.y);
		argData.getFlagArgument("-worldUp", 2, worldUp.z);
	}

	// STORE THE "normal" IF SPECIFIED, OTHERWISE ASSIGN DEFAULT:
	if (normalFlagSet)
	{
		argData.getFlagArgument("-normal", 0, normal.x);
		argData.getFlagArgument("-normal", 1, normal.y);
		argData.getFlagArgument("-normal", 2, normal.z);
	}

	if ( argData.isFlagSet( "-n" ) )
	{
		m_name = argData.flagArgumentString( "-n", 0, &status );
		CHECK_MSTATUS_AND_RETURN_IT( status );
	}
	else
	{
		m_name = "gRaycast";
	}

	status = createNode();

	if(!status){
		m_dagMod.undoIt();
	}

	return status;
}


MStatus RaycastCommand::redoIt()
{
	return m_dagMod.doIt();
}

MStatus RaycastCommand::undoIt()
{
	return  m_dagMod.undoIt();
}

MStatus RaycastCommand::getMeshShapeNode( MDagPath& path )
{
	MStatus status;

	if( path.apiType() == MFn::kMesh)
	{
		return MS::kSuccess;
	}

	unsigned int numShapes;
	status = path.numberOfShapesDirectlyBelow( numShapes );
	CHECK_MSTATUS_AND_RETURN_IT(status);
	for(unsigned int i=0; i<numShapes; i++){
		status = path.extendToShapeDirectlyBelow( i );
		CHECK_MSTATUS_AND_RETURN_IT(status);

		if( !path.hasFn(MFn::kMesh)){
			path.pop();
			continue;
		}

		MFnDagNode fnNode( path, &status );
		CHECK_MSTATUS_AND_RETURN_IT(status);
		if( !fnNode.isIntermediateObject()){
			return MS::kSuccess;
		}
		path.pop();

	}

	return MS::kFailure;
}

MStatus RaycastCommand::renameNodes(MObject transform, MString baseName)
{
	MStatus status;
	//  Rename the transform to something we know no node will be using.
	status = m_dagMod.renameNode(transform, "gRaycastCmdTemp");
	checkErr(status, "Could not rename transform node to temp name");
	//  Rename the mesh to the same thing but with 'Shape' on the end.
	MFnDagNode	 dagFn(transform);
	status = m_dagMod.renameNode(dagFn.child(0), "gRaycastCmdTempShape");
	checkErr(status, "Could not rename raycast node to temp name");
	
	MString  transformName = baseName + "#";
	status = m_dagMod.renameNode(transform, transformName);
	checkErr(status, "Could not rename transform node to final name");
	
	m_dagMod.doIt();
	checkErr(status, "Could not commit renaming of the nodes" );
	
	return status;
}
