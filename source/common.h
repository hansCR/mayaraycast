/*
 * common.h
 *
 *      Author: hans.cr
 */

#ifndef COMMON_H
#define COMMON_H

#include <maya/MPoint.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MBoundingBox.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>

#include <algorithm>

namespace MathUtils
{
	static double degToRad(double &degrees){
		return degrees * 0.0174533;
	}

	static double radToDeg(double &radians){
		return radians * 57.2958;
	}

	struct Vector {
		double x, y, z;

		Vector(double x, double y, double z):
			x(x), y(y), z(z)
		{}

		Vector(Vector& other):
			x(other.x),
			y(other.y),
			z(other.z)
		{}

		Vector(MPoint p):
			x(p.x), y(p.y), z(p.z)
		{}

		Vector(MFloatPoint p):
			x(p.x), y(p.y), z(p.z)
		{}

		Vector(MVector p):
			x(p.x), y(p.y), z(p.z)
		{}

		Vector(MFloatVector p):
			x(p.x), y(p.y), z(p.z)
		{}

		MPoint ToMPoint()
		{
			return MPoint(x,y,z,1.0);
		}
	};

	/// A ray with pre-calculated reciprocals to avoid divisions.
	struct Ray {
		Vector position;    	///< The origin of the ray.
		Vector direction_inv;	///< The inverse of each component of the ray's slope


		/// Precompute inverses for faster ray-box intersection tests.
		Ray(Vector position, Vector direction):
			position(position),
			direction_inv( 1.0/direction.x, 1.0/direction.y, 1.0/direction.z)
		{}

		Ray(Ray& other):
			position(other.position),
			direction_inv(other.direction_inv)
		{}

		bool Intersects(MBoundingBox& box)
		{
			// This is actually correct, even though it appears not to handle edge cases
			// (ray.n.{x,y,z} == 0).  It works because the infinities that result from
			// dividing by zero will still behave correctly in the comparisons.  Rays
			// which are parallel to an axis and outside the box will have tmin == inf
			// or tmax == -inf, while rays inside the box will have tmin and tmax
			// unchanged.

			double tx1 = (box.min().x - position.x) * direction_inv.x;
			double tx2 = (box.max().x - position.x) * direction_inv.x;

			double tmin = std::min(tx1, tx2);
			double tmax = std::max(tx1, tx2);

			double ty1 = (box.min().y - position.y) * direction_inv.y;
			double ty2 = (box.max().y - position.y) * direction_inv.y;

			tmin = std::max(tmin, std::min(ty1, ty2));
			tmax = std::min(tmax, std::max(ty1, ty2));

			double tz1 = (box.min().z - position.z) * direction_inv.z;
			double tz2 = (box.max().z - position.z) * direction_inv.z;

			tmin = std::min(tmin, std::min(tz1, tz2));
			tmax = std::min(tmax, std::max(tz1, tz2));

			return tmax >= std::max(0.0, tmin);
		}

	};

	class MatrixUtils
	{
		public:
			static void CreateMatrix(const MPoint& origin, const MVector& normal, const MVector& up,
					MMatrix& matrix){
				const MPoint& t = origin;
				const MVector& y = normal;
				MVector x = y ^ up;
				MVector z = x ^ y;
				matrix[0][0] = x.x; matrix[0][1] = x.y; matrix[0][2] = x.z; matrix[0][3] = 0.0;
				matrix[1][0] = y.x; matrix[1][1] = y.y; matrix[1][2] = y.z; matrix[1][3] = 0.0;
				matrix[2][0] = z.x; matrix[2][1] = z.y; matrix[2][2] = z.z; matrix[2][3] = 0.0;
				matrix[3][0] = t.x; matrix[3][1] = t.y; matrix[3][2] = t.z; matrix[3][3] = 1.0;
			}

			static void CreateMatrix(const MPoint& origin, const MVector& x, const MVector& y, const MVector& z,
					MMatrix& matrix){
				const MPoint& t = origin;

				matrix[0][0] = x.x; matrix[0][1] = x.y; matrix[0][2] = x.z; matrix[0][3] = 0.0;
				matrix[1][0] = y.x; matrix[1][1] = y.y; matrix[1][2] = y.z; matrix[1][3] = 0.0;
				matrix[2][0] = z.x; matrix[2][1] = z.y; matrix[2][2] = z.z; matrix[2][3] = 0.0;
				matrix[3][0] = t.x; matrix[3][1] = t.y; matrix[3][2] = t.z; matrix[3][3] = 1.0;
			}

			static void getWorldBBox(const MDagPath dagPath, MBoundingBox& bbox){
				MFnDagNode dagNodeFn(dagPath);
			    bbox = dagNodeFn.boundingBox();
			    MMatrix m = dagPath.exclusiveMatrix();
			    bbox.transformUsing(m);
			}
	};
};


#endif
