/*
 * pluginMain.cpp
 *
 *      Author: hans.cr
 */

#include <maya/MFnPlugin.h>

#include "constants.h"
#include "raycastLocator.h"
#include "raycastCommand.h"
#include "collisionDeformer.h"
#include "collisionCommand.h"

MStatus initializePlugin( MObject obj )
{
    MStatus status;

    MFnPlugin fnPlugin( obj, AUTHOR, PLUGIN_VERSION, "Any" );

    status = fnPlugin.registerNode( "gRaycast", RaycastLocator::id,
    		RaycastLocator::creator, RaycastLocator::initialize, MPxNode::kLocatorNode );
    CHECK_MSTATUS_AND_RETURN_IT( status );

    status = fnPlugin.registerNode( "gCollisionDeformer", CollisionDeformer::id,
    		CollisionDeformer::creator, CollisionDeformer::initialize, MPxNode::kDeformerNode );
    CHECK_MSTATUS_AND_RETURN_IT( status );

    status = fnPlugin.registerCommand( "gRaycast", RaycastCommand::creator, RaycastCommand::newSyntax );
    CHECK_MSTATUS_AND_RETURN_IT( status );

    status = fnPlugin.registerCommand( "gCollisionDeformer", CollisionCommand::creator, CollisionCommand::newSyntax );
    CHECK_MSTATUS_AND_RETURN_IT( status );

	status = fnPlugin.registerUI("gMayaRaycastCreateUI", "gMayaRaycastDeleteUI");
	if (!status) {
		status.perror("registerUI");
		return status;
	}
    
    return MS::kSuccess;
}


MStatus uninitializePlugin( MObject obj )
{
    MStatus status;

    MFnPlugin fnPlugin( obj );

    status = fnPlugin.deregisterNode( RaycastLocator::id );
    status = fnPlugin.deregisterNode( CollisionDeformer::id );
    status = fnPlugin.deregisterCommand( "gRaycast" );
    status = fnPlugin.deregisterCommand( "gCollisionDeformer" );
    CHECK_MSTATUS_AND_RETURN_IT( status );

    return MS::kSuccess;
}
