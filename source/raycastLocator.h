/*
 * raycastLocator.h
 *
 *      Author: hans.cr
 */

#ifndef RAYCASTLOCATOR_H
#define RAYCASTLOCATOR_H

#include <map>

#include <maya/MPxLocatorNode.h>
#include <maya/MTypeId.h>
#include <maya/MGlobal.h>
#include <maya/MObject.h>
#include <maya/MDagPath.h>
#include <maya/MBoundingBox.h>

#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>

#include <maya/MFloatVector.h>
#include <maya/MVector.h>
#include <maya/MFloatPoint.h>
#include <maya/MPoint.h>
#include <maya/MFloatMatrix.h>
#include <maya/MMatrix.h>
#include <maya/MFloatPointArray.h>
#include <maya/MEulerRotation.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>

#include "common.h"



class RaycastLocator : public MPxLocatorNode
{
public:
						RaycastLocator();
	virtual void		postConstructor();
	virtual				~RaycastLocator();

	static  void*		creator();

	virtual	MStatus		setDependentsDirty(const MPlug& inPlug, MPlugArray& affectedPlugs);
	// virtual MStatus		connectionBroken(const MPlug& plug, const MPlug& otherPlug, bool asSrc);
	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	virtual void		draw( M3dView&, const MDagPath&, M3dView::DisplayStyle, M3dView::DisplayStatus );

	virtual bool		isBounded() const;
	virtual MBoundingBox boundingBox() const;

	static  MStatus		initialize();

    static	MObject		aOutTranslate;
    static	MObject		aRotateX;
    static	MObject		aRotateY;
    static	MObject		aRotateZ;
    static	MObject		aOutRotate;
    static	MObject		aOutMatrix;
    static	MObject		aIsHit;
    static	MObject		aParamLen;

    static	MObject		aInMatrix;
    static	MObject		aRayLen;
	static	MObject		aMeshes;
	static	MObject		aTargetParentInverse;
	static	MObject		aTargetRotateOrder;
	static	MObject		aAim;
	static	MObject		aUp;
	static	MObject		aWorldUp;
	static	MObject		aNormal;


	static	MTypeId		id;

private:
	MFloatPoint fpHit, fpSource, fpDir;
	MVector vAim, vUpIn, vWorldUp, vNormal;
	MTransformationMatrix::RotationOrder roo;
	MMatrix mParentInverse;

	unsigned int closestIndex;
	bool m_initialized, m_dirtyRoO;
	std::map<unsigned int, bool> m_dirtyMeshes;
	std::map<unsigned int, MMeshIsectAccelParams> m_accelParams;
	std::map<unsigned int, MObject> m_meshes;
};

#endif
