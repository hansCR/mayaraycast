/*
 * raycastCommand.h
 *
 *      Author: hans.cr
 */

#ifndef COLLISIONCOMMAND_H
#define COLLISIONCOMMAND_H

#include <maya/MPxCommand.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MDagPath.h>
#include <maya/MDagModifier.h>
#include <maya/MSelectionList.h>
#include <maya/MVector.h>
#include <maya/MString.h>
#include <maya/MPlug.h>

#include <maya/MFnDagNode.h>
#include <maya/MFnDependencyNode.h>

#include "common.h"

class CollisionCommand : public MPxCommand
{
public:
					CollisionCommand();
	virtual 		~CollisionCommand();
	static	void*	creator();
	static	MSyntax	newSyntax();
	virtual MStatus	doIt( const MArgList& argList );
	virtual MStatus	redoIt();
	virtual MStatus	undoIt();
	virtual bool	isUndoable() const;


private:
	bool nameFlagSet, queryFlagSet;
	MDGModifier m_dgMod;
	MString m_name;
	MSelectionList sList;
	MDagPath m_pathColliderMesh, m_pathDeformMesh;

	MStatus getMeshShapeNode( MDagPath& path );
	MStatus getDeformerFromBaseMesh( MObject& oDeformer );
};

#endif
