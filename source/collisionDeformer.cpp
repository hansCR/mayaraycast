/*
 * collisionDeformer.cpp
 *
 *  Created on: Feb 29, 2016
 *      Author: hans.cr
 */

#include "collisionDeformer.h"

MTypeId     CollisionDeformer::id( 0x00125610 );

MObject     CollisionDeformer::aCollider;
MObject     CollisionDeformer::aStartFrame;
MObject     CollisionDeformer::aBulgeExtend;
MObject     CollisionDeformer::aBulge;
MObject     CollisionDeformer::aOffset;
MObject     CollisionDeformer::aColliderBBoxSize;
MObject     CollisionDeformer::aColliderMatrix;
MObject     CollisionDeformer::aBulgeShape;
MObject     CollisionDeformer::aBackFace;
MObject     CollisionDeformer::aSculptMode;

CollisionDeformer::CollisionDeformer()
{
	m_initialized = false;
}


void CollisionDeformer::postConstructor()
{
	MObject oThis = thisMObject();
	MFnDependencyNode fnNode( oThis );

    MRampAttribute bulgeShapeHandle(oThis, aBulgeShape);

    MFloatArray a1, b1;
    MIntArray c1;

    a1.append(0.0f);
    a1.append(0.2f);
    a1.append(1.0f);

    b1.append(0.0f);
	b1.append(1.0f);
	b1.append(0.0f);

	c1.append(MRampAttribute::kSpline);
	c1.append(MRampAttribute::kSpline);
	c1.append(MRampAttribute::kSpline);

    bulgeShapeHandle.addEntries(a1, b1, c1);
}


CollisionDeformer::~CollisionDeformer()
{
}


void* CollisionDeformer::creator()
{
    return new CollisionDeformer();
}


MStatus	CollisionDeformer::setDependentsDirty(const MPlug& plug, MPlugArray& affectedPlugs)
{
	MStatus status;

	MObject oThis = thisMObject();

	if(plug == aCollider ){
		m_dirtyCollider = true;
	}

	if(plug == inputGeom ){
		m_dirtyInput = true;
	}

	return MS::kSuccess;
}


MStatus CollisionDeformer::deform( MDataBlock& data, MItGeometry& itGeo,
		const MMatrix& localToWorldMatrix, unsigned int geomIndex )

{
	MStatus status;
	MObject oThis = thisMObject();

	MTime currentFrame = MAnimControl::currentTime();

	MMatrix worldToLocalMatrix = localToWorldMatrix.inverse();

	float tempRawMatrix[4][4];
	localToWorldMatrix.get(tempRawMatrix);
	MFloatMatrix localToWorldFloatMatrix(tempRawMatrix);
	MFloatMatrix worldToLocalFloatMatrix = localToWorldFloatMatrix.inverse();


	// Get attribute data
	float env = data.inputValue( envelope ).asFloat();
	double offset = data.inputValue(aOffset).asDouble();
	double bulgeExtend = data.inputValue(aBulgeExtend).asDouble();
	int startFrame = data.inputValue(aStartFrame).asInt();

	double bulge = data.inputValue(aBulge).asDouble();
	MRampAttribute bulgeShape(oThis, aBulgeShape, &status);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	bool sculptMode = data.inputValue(aSculptMode).asBool();
	bool backFace = data.inputValue(aBackFace).asBool();


	if(env==0){
		return MS::kSuccess;
	}

	// Get input geometry data
	MArrayDataHandle hInput = data.outputArrayValue( input, &status);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	status = hInput.jumpToElement(geomIndex);
	MDataHandle hInputElement = hInput.outputValue(&status);
	MObject oInputGeom = hInputElement.child(inputGeom).asMesh();


	MFnMesh fnMesh(oInputGeom, &status);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	status = fnMesh.getPoints(pointPositions);
	status = fnMesh.getVertexNormals(false, pointNormals);
	MFloatArray bulgeParameter(pointPositions.length());
	if(currentFrame.value() == startFrame || !sculptMode || !m_initialized){
		itGeo.allPositions(deformedPoints);
		// cout << "reset points" << endl;
	}


	CHECK_MSTATUS_AND_RETURN_IT(status);
	m_dirtyInput = false;



	//Get collision data
	MObject oCollider = data.inputValue( aCollider, &status).asMesh();
	CHECK_MSTATUS_AND_RETURN_IT(status);

	if(oCollider.isNull()){
		return MS::kSuccess;
	}

	MMatrix colliderMatrix = data.inputValue( aColliderMatrix, &status).asMatrix();
	colliderMatrix.get(tempRawMatrix);
	MFloatMatrix colliderFloatMatrix(tempRawMatrix);
	MFloatMatrix colliderFloatMatrixInverse = colliderFloatMatrix.inverse();


	CHECK_MSTATUS_AND_RETURN_IT(status);
	MVector colliderBBoxSize = data.inputValue( aColliderBBoxSize, &status).asVector();
	double colliderBBoxMagnitude = colliderBBoxSize.length();
	CHECK_MSTATUS_AND_RETURN_IT(status);

	MFnMesh fnCollider(oCollider, &status);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	float w;

	if (m_dirtyCollider || !m_initialized) {
		// cout << "recalc collider" << endl;
		intersector.create(oCollider);
		mmAccelParams = fnCollider.autoUniformGridParams();
		m_dirtyCollider = false;
	}



	// Initialize variables for rays
	MFloatPointArray hitPoints;
	MFloatArray hitRayParams, hitBary1s, hitBary2s;
	MIntArray hitFaces, hitTriangles;
	float bboxSize = colliderBBoxSize.length();
	float maxRayParam = bboxSize*2;
	float maxCloseParam = bboxSize*0.5f;

	float maxdist = 0.0f;


	MPoint point;
	MPointOnMesh pointInfo;


	bool anyCollission = false;


	for( ; !itGeo.isDone(); itGeo.next() )
	{
		w = weightValue(data, geomIndex, itGeo.index());
		if (w == 0) {
			continue;
		}
		// global attributes:
		// X env		float	a multiplier for the overall deformation
		// X bulge		double	a multiplier for the bulge amount
		// X maxDeform	float	The maximum distance a point has been moved by collision

		// per point attributes:
		// inPosition
		// normal
		// weight
		// postCollissionPoint	same as in-position, set to closestPoint when point is affected by collision
		// X bulgeParameter		a multiplier for the bulge based on the bulgeExtend, bulgeShape and distanceToMesh
		// out-position = envelope * weight * (postCollissionPoint + bulgeParameter * normal * bulge * maxDeform)

		// local attributes:
		// X distanceToMesh		the distance from in-position to the mesh - set to 0 when point is affected by collision

		//get point-location

		int i = itGeo.index();


		MFloatPoint pointPosition = MFloatPoint(deformedPoints[i]);
		MFloatVector pointNormal = pointNormals[i];
		MFloatPoint closestPoint;

		pointPosition *= localToWorldFloatMatrix * colliderFloatMatrix;
		pointNormal *= localToWorldFloatMatrix;

		float distanceToMesh;
		bool outOfRange = false;
		
		status = intersector.getClosestPoint(MPoint(pointPosition), pointInfo, bulgeExtend);
		if(status){
			closestPoint = pointInfo.getPoint();
	
			pointPosition*=colliderFloatMatrixInverse;
			closestPoint*=colliderFloatMatrixInverse;
	
			distanceToMesh = closestPoint.distanceTo(pointPosition);
			pointPosition*=colliderFloatMatrix;
		}
		else{
			outOfRange = true;
			distanceToMesh = (float)bulgeExtend;
		}

		

		bool gotHit = fnCollider.allIntersections( pointPosition, pointNormal, NULL, NULL, true,
				MSpace::kWorld, maxRayParam, false, &mmAccelParams, true, hitPoints, &hitRayParams,
				&hitFaces, &hitTriangles, &hitBary1s, &hitBary2s, 1e-6f, &status);

		if(gotHit){
			unsigned int hitCount = hitPoints.length();
			//if ray hits an odd number of times we are inside the volume
			if (hitCount & 1 && hitRayParams[0] < bboxSize){

				anyCollission = true;

				if(outOfRange){

					intersector.getClosestPoint(MPoint(pointPosition), pointInfo, maxCloseParam);
					closestPoint = pointInfo.getPoint();

					pointPosition*=colliderFloatMatrixInverse;
					closestPoint*=colliderFloatMatrixInverse;

					distanceToMesh = closestPoint.distanceTo(pointPosition);

				}
				else
				{
					pointPosition*=colliderFloatMatrixInverse;
				}
				pointPosition*=worldToLocalFloatMatrix;
				closestPoint*=worldToLocalFloatMatrix;

				if(maxdist < distanceToMesh){
					maxdist = distanceToMesh;
				}
				distanceToMesh = 0.0f;
				deformedPoints[i] = pointPosition + w * env * (closestPoint - pointPosition);

			}

		}

		bulgeShape.getValueAtPosition(float(distanceToMesh/bulgeExtend), bulgeParameter[i]);
	}

	if(anyCollission){
		itGeo.reset();
		for( ; !itGeo.isDone(); itGeo.next() ){
			int i = itGeo.index();
			w = weightValue(data, geomIndex, i);
			MFloatVector pointNormal = pointNormals[i] * localToWorldFloatMatrix;
			pointNormal.normalize();
			pointNormal *= worldToLocalFloatMatrix;

			deformedPoints[i] += bulgeParameter[i] * maxdist * w * env * bulge * pointNormal;
		}
	}

	itGeo.setAllPositions(deformedPoints);


	m_initialized = true;

    return MS::kSuccess;
}


MStatus CollisionDeformer::initialize()
{
    MStatus status;

    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MRampAttribute rAttr;

	aCollider = tAttr.create("collider", "coll", MFnData::kMesh );
	tAttr.setHidden(true);
	addAttribute(aCollider);
	attributeAffects(aCollider, outputGeom );

	aStartFrame = nAttr.create("startFrame", "sf", MFnNumericData::kInt, 1);
	nAttr.setKeyable(false);
	nAttr.setChannelBox(true);
	nAttr.setStorable(true);
	addAttribute(aStartFrame);
	attributeAffects(aStartFrame, outputGeom );

	aBulgeExtend = nAttr.create("bulgeExtend", "bex", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setMin(1e-6f);
	nAttr.setSoftMax(10);
	addAttribute(aBulgeExtend);
	attributeAffects(aBulgeExtend, outputGeom );

	aBulge = nAttr.create( "bulge", "blg", MFnNumericData::kDouble, 1.0);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setSoftMin(0);
	nAttr.setSoftMax(10);
	addAttribute(aBulge);
	attributeAffects(aBulge, outputGeom );

	aOffset = nAttr.create( "offset", "off", MFnNumericData::kDouble, 0.0);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setSoftMin(0);
	nAttr.setSoftMax(1);
	addAttribute(aOffset);
	attributeAffects(aOffset, outputGeom );

	aColliderBBoxSize = nAttr.createPoint("colliderBBox", "cbb");
	addAttribute(aColliderBBoxSize);
	attributeAffects(aColliderBBoxSize, outputGeom );

	aColliderMatrix = mAttr.create("colliderMatrix","collMatr");
	addAttribute(aColliderMatrix);
	attributeAffects(aColliderMatrix, outputGeom );

	aBulgeShape = rAttr.createCurveRamp("bulgeshape", "blgshp");
	addAttribute(aBulgeShape);
	attributeAffects(aBulgeShape, outputGeom );

	aBackFace = nAttr.create("backface_culling", "bkcul", MFnNumericData::kBoolean, false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	addAttribute(aBackFace);
	attributeAffects(aBackFace, outputGeom );

	aSculptMode = nAttr.create("sculpt_mode", "snmd", MFnNumericData::kBoolean, false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	addAttribute(aSculptMode);
	attributeAffects(aSculptMode, outputGeom );

	MGlobal::executeCommand( "makePaintable -attrType multiFloat -sm deformer gCollisionDeformer weights;" );

	return MS::kSuccess;
}

